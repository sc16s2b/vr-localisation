Absolute Localisation Test
NameNotSet, Age: AgeNotSet, 18:48, 2/5/2020
Random Source Location: true, Number of Tests: 10, Initial Delay: 5.0, Sound Duration: 1.0, Minimum Delay: 5, Maximum Delay: 10
Delay: 5.0, Source Location: X=-133.183 Y=-213.652 Z=488.849, Estimate Distance from Source: 254.954559
Delay: 9.0, Source Location: X=-241.982 Y=6.331 Z=289.320, Estimate Distance from Source: 264.627319
Delay: 7.0, Source Location: X=-212.052 Y=159.267 Z=158.940, Estimate Distance from Source: 218.558792
Delay: 8.0, Source Location: X=-367.318 Y=413.785 Z=592.884, Estimate Distance from Source: 204.497223
Delay: 7.0, Source Location: X=-201.186 Y=299.849 Z=198.842, Estimate Distance from Source: 114.805122
Delay: 8.0, Source Location: X=-215.533 Y=433.580 Z=152.192, Estimate Distance from Source: 386.284882
Delay: 5.0, Source Location: X=-458.877 Y=74.783 Z=407.515, Estimate Distance from Source: 516.563599
Delay: 9.0, Source Location: X=-334.776 Y=321.552 Z=62.036, Estimate Distance from Source: 371.199951
Delay: 7.0, Source Location: X=-112.069 Y=205.285 Z=77.102, Estimate Distance from Source: 118.278076
Delay: 5.0, Source Location: X=-427.964 Y=90.057 Z=292.745, Estimate Distance from Source: 451.04303
